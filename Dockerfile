FROM python:3.7-alpine
LABEL maintainer="CERN Authoring <authoring@cern.ch>"
LABEL version="1.0.4"
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY mkdocs_configuration /usr/local/bin/
RUN chmod 755 /usr/local/bin/mkdocs_configuration
COPY mkdocs_material_nested_lists_rendering_fix /usr/local/bin/
RUN chmod 755 /usr/local/bin/mkdocs_material_nested_lists_rendering_fix
COPY mkdocs_configuration_lib.py /usr/local/bin/
COPY mkdocs_configuration_tests.py /usr/local/bin/
