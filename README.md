![Deprecated](deprecated.png "Deprecated")

This how-to has been **DEPRECATED**. Please go instead to [https://how-to.docs.cern.ch](https://how-to.docs.cern.ch)

# MkDocs

> [MkDocs](https://www.mkdocs.org/) is a fast, simple and downright gorgeous static site generator that's geared towards building project documentation. Documentation source files are written in Markdown, and configured with a single YAML configuration file.

This repository provides resources and information on how to use MkDocs to build your own documentation. Feel free to follow all the suggested steps or pick and choose the ones that you need.

## How-to

### GitLab repository

Start by [creating a new GitLab repository](https://gitlab.cern.ch/projects/new) to host your source Markdown files and MkDocs configuration. Your GitLab repository should be structured as follows:

```
docs/
    (Your Markdown files go here following
    the directory structure of your choice)
mkdocs.yml
```

In more detail:
* All your Markdown files should be in the `docs` directory so that MkDocs knows where to find them when building your documentation.
* Your specific configuration for MkDocs goes into `mkdocs.yml`, a special `YAML` file that MkDocs parses. _Keep reading to find out what goes in `mkdocs.yml`_.

More information on this is available in the [respective MkDocs documentation](https://www.mkdocs.org/user-guide/writing-your-docs/).

Therefore, the simplest actual structure would look like this:
    
```
docs/
    index.md
mkdocs.yml
```

A slightly more complicated structure would look like this:

```
docs/
    index.md
    about.md
    chapter_01.md
    chapter_02/
        index.md
        foo.md
        bar.md
    images/
        image_01.jpg
        image_02.png
mkdocs.yml
```

An example GitLab repository with a simple structure for MkDocs is available here: [gitlab.cern.ch/authoring/documentation/mkdocs-example](https://gitlab.cern.ch/authoring/documentation/mkdocs-example).

**Note** that there are no restictions for the _Visibility Level_ of your GitLab project (_Private_, _Internal_ or _Public_).

### EOS folder

Before moving on to building and deploying your documentation, you need to have a website prepared to deploy it on. [Create a new website of **Site type** "**EOS folder**"](https://webservices.web.cern.ch/webservices/Services/CreateNewSite/Default.aspx). The **Site category** should be **Official** and the **Site name** (example: `my-project-documentation`) and **Description** (example: _My Project Documentation_) are up to your discretion. The **Path** should be an actual path on EOS that you have access to (example: `/eos/project/m/my_project/www/documentation/`). More information on this is available in the [CERNBox Manual](https://cernbox-manual.web.cern.ch/cernbox-manual/en/web/project_website_content.html); if you still have questions please address them at the [CERN Service Portal](https://cern.service-now.com/service-portal/).

It is highly recommended that you create a [Service account](https://account.cern.ch/account/Help/?kbid=011010) and give access to this Service account on the EOS folder you plan to deploy your documentation on. More information on this is available in the [CERNBox Manual](https://cernbox-manual.web.cern.ch/cernbox-manual/en/sharing/share_a_folder.html#link-share-upload-only). _Keep reading to find out where you will use this account_.

### GitLab Continuous Integration & Delivery (CI/CD)

Once your GitLab repository structure is in place, your Markdown files are there and your website prepared, you want to actually build and deploy your documentation. This process can be automated with minimum configuration using GitLab's integrated Continuous Integration & Delivery (CI/CD) pipelines. To achieve this, all you need is to add a `.gitlab-ci.yml` file in your GitLab repository with the following contents:

```yaml
stages:
    - build
    - deploy
building:
    stage: build
    image: gitlab-registry.cern.ch/authoring/documentation/mkdocs:stable
    script:
        - mkdocs build --clean --site-dir public
    artifacts:
        paths:
            - public
        expire_in: 1 hour
deploying:
    stage: deploy
    image: gitlab-registry.cern.ch/ci-tools/ci-web-deployer:latest
    only:
        - master
    script:
        - deploy-eos
```

The above CI/CD configuration builds and deploys your documentation in 2 distinct stages:

In the `building` stage, notice the `image` used is the `gitlab-registry.cern.ch/authoring/documentation/mkdocs:stable` one. This a docker image that contains all the necessary MkDocs resources that can build your documentation. The command `mkdocs build --clean --site-dir public` under `script` is the one that actually builds your documentation.

If you want to do more things alongside building your documentation you can add more commands under `script`. Just make sure they are supported by the docker image. For example you can copy a `.htaccess` file to define custom authentication rules for your documentation if your documentation is deployed on an EOS folder (_Keep reading to find out more on that_).

The `gitlab-registry.cern.ch/authoring/documentation/mkdocs:stable` docker image is one to use for production documentation but there are more options to choose from:
* `gitlab-registry.cern.ch/authoring/documentation/mkdocs:latest` Latest image (well-tested new features): only use for production if you are **adventurous**.
* `gitlab-registry.cern.ch/authoring/documentation/mkdocs:stable` Stable image (stable features; synonym for the most recent tagged image): **use this one for production**.
* `gitlab-registry.cern.ch/authoring/documentation/mkdocs:<tag>` Tagged image ("1.0", "1.1", etc) that refers to a specific stable version: use this one for production if you want a **specific version**.
* `gitlab-registry.cern.ch/authoring/documentation/mkdocs:<dev*>` Development images (untested new features): do **not** use for production.

Check out the respective [GitLab Container Registry](https://gitlab.cern.ch/authoring/documentation/mkdocs/container_registry) for a complete list.

In the `deploying` stage, notice the `image` used is the `gitlab-registry.cern.ch/ci-tools/ci-web-deployer:latest` one. This is a docker image that can access an EOS folder (among others) and deploy files in it. To make sure it knows which EOS folder to deploy in and what username and password to use to access that folder, you have to define the following GitLab CI/CD Variables: `EOS_ACCOUNT_USERNAME`, `EOS_ACCOUNT_PASSWORD` and `EOS_PATH`. The `EOS_ACCOUNT_USERNAME` and `EOS_ACCOUNT_PASSWORD` correspond to the Service account that you previously created and the `EOS_PATH` correspons to the path on EOS you previously defined for your website. More information on this is available at the [`gitlab-registry.cern.ch/ci-tools/ci-web-deployer` docker image documentation](https://gitlab.cern.ch/ci-tools/ci-web-deployer).

By now, your GitLab repository should be structured as follows:

```
docs/
    (Your Markdown files go here following
    the directory structure of your choice)
mkdocs.yml
.gitlab-ci.yml
```

Here is the [.gitlab-ci.yml](https://gitlab.cern.ch/authoring/documentation/mkdocs-example/blob/master/.gitlab-ci.yml) file in the [example GitLab repository](https://gitlab.cern.ch/authoring/documentation/mkdocs-example) previously mentioned.

### Authentication

You might want to restrict the access to your documentation to specific CERN e-groups. This can easily be achieved in websites of **EOS folder** type with the addition of a `.htaccess` file as documented [here](https://espace.cern.ch/webservices-help/websitemanagement/ConfiguringAFSSites/Pages/AccesscontrolonAFSsites.aspx).

Add a `.htaccess` file in your GitLab repository:

```
docs/
    (Your Markdown files go here following
    the directory structure of your choice)
mkdocs.yml
.gitlab-ci.yml
.htaccess
```

with the following contents:

```
SSLRequireSSL
AuthType shibboleth
ShibRequireSession On
ShibRequireAll On
ShibExportAssertion Off
Require valid-user
Require ADFS_GROUP "e-group-foo" "e-group-bar"
```

Instruct your CI/CD configuration to copy the `.htaccess` file into the directory that will be deployed (`cp .htaccess public/` under `script`):

```yaml
stages:
    - build
    - deploy
building:
    stage: build
    image: gitlab-registry.cern.ch/authoring/documentation/mkdocs:stable
    script:
        - mkdocs build --clean --site-dir public
        - cp .htaccess public/
    artifacts:
        paths:
            - public
        expire_in: 1 hour
deploying:
    stage: deploy
    image: gitlab-registry.cern.ch/ci-tools/ci-web-deployer:latest
    only:
        - master
    script:
        - deploy-eos
```

### MkDocs configuration

Once the infrastructure is in place you want to focus on writing your documentation and configuring MkDocs according to your needs. MkDocs has excellent [documentation](https://www.mkdocs.org/) that you can look through.

As a minimum the `mkdocs.yml` configuration file must contain the `site_name` setting. All other settings are optional and documented [here](https://www.mkdocs.org/user-guide/configuration/). Here is a [simple example](https://gitlab.cern.ch/authoring/documentation/mkdocs-example/blob/master/mkdocs.yml) with a few more settings defined:

```
site_name: MkDocs Example
site_description: MkDocs Example
site_author: CERN Authoring
site_url: https://cern.ch/mkdocs-example
repo_name: GitLab
repo_url: https://gitlab.cern.ch/authoring/documentation/mkdocs-example
theme:
    name: material
nav:
    - Introduction: index.md
    - 'Chapter 1': chapter_1.md
    - 'Chapter 2': chapter_2/index.md
    - 'MkDocs official documentation': https://www.mkdocs.org/
```

In the `gitlab-registry.cern.ch/authoring/documentation/mkdocs:stable` docker image, the `mkdocs`, `readthedocs` and `material` themes are available (define under `theme` / `name`). Respective examples: [`mkdocs` theme](https://www.mkdocs.org/), [`readthedocs` theme](https://example.readthedocs.io) and [`material` theme](https://cern.ch/mkdocs-example).

The `nav` setting allows you to take full control over the structure of your documentation but you can safely skip it and MkDocs will automatically create an alphanumerically sorted, nested list of all the Markdown files found within the `docs` directory and its sub-directories.

The available [formatting options via the `markdown_extensions` setting](https://www.mkdocs.org/user-guide/configuration/#formatting-options) are especially interesting. The [PyMdown Extensions](https://facelessuser.github.io/pymdown-extensions) package is also installed. See for example [this page in CERN Search Documentation](https://cern.ch/cernsearchdocs/example/) that uses extentions to highlight code and nest code blocks likes this:

```
markdown_extensions:
    - admonition
    - pymdownx.superfences
    - toc:
        permalink: true
```
## Custom features

### Automatically generating the MkDocs configuration

For usage of the `mkdocs_configuration` CLI please run `docker run -i -t gitlab-registry.cern.ch/authoring/documentation/mkdocs mkdocs_configuration --help`.

### Better rendering of nested lists for the Material theme

The `mkdocs_material_nested_lists_rendering_fix` CLI addresses the issue described [here](https://github.com/squidfunk/mkdocs-material/issues/508). To use it just add `- mkdocs_material_nested_lists_rendering_fix` under `script` in the `building` stage in your CI configuration (`.gitlab-ci.yml` file) right before `- mkdocs build --clean --site-dir public`.

## Contributing

Read beyond this point if you are interested in contributing in this project. Get in touch at <authoring@cern.ch>.

* Development takes place in the `<dev*>` branches. When pushed (`$ git push <dev*>`), the `<dev*>` branch triggers the `<dev*>`-tagged docker images that users should **not** use for production.
* When the `<dev*>` branch is in a stable state we can clean up our commits (`$ git rebase -i master`), switch to the `master` branch (`$ git checkout master`) and `merge` the `<dev*>` branch into the `master` branch (`$ git merge <dev*>`). When pushed (`$ git push master`), the `master` branch triggers the `latest`-tagged docker images that adventurous users can use for production.
* When the `master` branch is in a stable state we can switch to the `stable` branch (`$ git checkout stable`) and bring it up to date with the `master` branch (`$ git rebase master`). When pushed (`$ git push stable`), the `stable` branch triggers the `stable`-tagged docker images that all users can safely use for production.
* In addition to the `stable`-tagged docker images we can also tag docker images with the specific version identifier for this release by tagging the latest commit in the `stable` branch accordingly (`$ git tag -a "1.0" -m "Version 1.0"`).

The `master` branch is built like this:
```yaml
# when building master, update the default `latest` tag for the docker image
Build latest:
  stage: build
  only:
    - master
  tags:
    - docker-image-build
  script: "echo 'Building latest docker image'"  
```

All other branches (`<dev*>`, `stable`) and tags are built like this:
```yaml
# when building a topic branch or tag, use the branch/tag name as the docker image tag
Build branch or tag:
  stage: build
  except:
    - master
  tags:
    - docker-image-build
  script: "echo 'Building docker image based on its branch or tag'"
  variables:
    TO: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_NAME}
```
